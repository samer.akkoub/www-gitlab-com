---
layout: markdown_page
title: Working with Trial Extensions
category: License and subscription
---

### On this page
{:.no_toc}

- TOC
{:toc}

----

## Overview

Use these workflows if you need to extend a GitLab trial.

## Extending GitLab.com Trials

[Customer Console Workflow](https://about.gitlab.com/handbook/support/workflows/customer_console.html)

## Extending GitLab Self-managed Trials
Self-managed trials cannot be extended -  a license must be re-issued and applied to the instance in order to "extend" a trial.
Self-managed licenses are managed in the [license app](https://license.gitlab.com). In order to sign-in, you will need an account on `dev.gitlab.org`. If you don't have one, open an [access request](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issuable_template=Single_Person_Access_Request) and refer that your role entitles you to access with [this link](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/blob/master/.gitlab/issue_templates/role_baseline_access_request_tasks/department_customer_support/role_support_engineer.md).

To re-issue a license:
1. Log in to the [license app](https://license.gitlab.com)
1. Identify the trial license by searching by customer or company name.
1. Click the name on the license to view its details.
1. In the **Edit license** section, choose _Duplicate license_
1. Modify the `Users count` and `Expires at` fields, consider adding a note to describe why you're issuing this license.
1. Click **Create license**

The license should be emailed out immediately, but you're also welcome to download it and pass it along to the customer directly.